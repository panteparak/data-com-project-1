# Welcome to ICCS 223 Data Communication and Network: Project 1
## Resumable Concurrent File Download

Features:
------
* Redirect
* Large File size Downloads (100 MB+)
* ~~Chunk Transfer~~
* Resumable Downloads (with Concurrent)
* Concurrent Downloads (Cannot change Concurrent number)
* Auto-Create "SRGetDownloader" folder in root directory of this project, Downloaded files are in there.
* JSON File Storage (Header)
* * *

TO RUN:
------
You must permit executable permission in order to run this downloader

Go to the project's current directory and run this command through your favourite Terminal app or Command-line (Windows)

### For Mac or Linux: ###
Permit executable rights to file system  
`chmod +x srget` OR `sudo chmod +x srget` 

Run the program  
`./srget -o <output file> [-c [<numConn>]] http://someurl.domain[:port]/path/to/file`

### For Windows: ###

                  ██████████
                  ██████████
                  ██████████
               ▄▄▄██████████▄▄▄
                 ▄▀░░░░░░░░▀▄
                ▐░░▄▀▀░░▀▀▄░░▌
                ▐░░██▀░░▀██░░▌
                ▐░░░░░▀▀░░░░░▌
                ▐░░░░░░░░░░░░▌
                ▐░░░▄▀▀▀▀▄░░░▌
                 ▀▄░░░▀▀░░░▄▀
                 ▐▒▀▄▄▄▄▄▄▀▒▌
          ▄▄▄▄▄▄▀▀▒▓▓▓▓▓▓▓▓▒▀▀▄▄▄▄▄▄
        ▄▀▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▀▄
       ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓
      ▓▒▒▒▒▒▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▒▒▒▒▒▓
     ▓▒▒▒▒▒▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▒▒▒▒▒▓
    ▓▒▒▒▒▒▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▒▒▒▒▒▓

Run through CMD  `python srget -o <output file> [-c [<numConn>]] http://someurl.domain[:port]/path/to/file`
* * *